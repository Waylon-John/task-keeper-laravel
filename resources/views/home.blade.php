@extends('layouts.main')

@section('content')
    <div class="section">
        <h3 class="blue-text">Hello, {{auth()->user()->name}}</h3>
    </div>
    <div class="row">
        {!!Form::open(['action'=>'TasksController@store','method'=>'POST'])!!}
        <div class="input-field col s10">
            <i class="material-icons prefix">note_add</i>
            {!!Form::text('task','')!!}
        </div>
        <div class="input-field col s2">
            {!!Form::submit('Pin Task',['class'=>'blue btn-small'])!!}
        </div>
        {!!Form::close()!!}
    </div>
    <div class="row center section">
        <h4 class="blue-text">My Pinned Tasks</h4>
    </div>
    <div class="row">
        <ol>
            @if(count($task)>0)
                @foreach($task as $userTask)
                    <li>
                        {{$userTask->task}}
                        <span class="right">
                            <button class="blue btn-small"><a class="white-text" href="/tasks/{{$userTask->id}}/edit">Edit Task</a></button>&nbsp
                            {!!Form::open(['action' => ['TasksController@destroy',$userTask->id] , 'method' => 'POST' , 'class' => 'right']) !!}
                                {!!Form::hidden('_method','DELETE')!!}
                                <button class="red btn-small" type="submit">Unpin Task</button>
                            {!!Form::close()!!}  
                        </span>
                    </li>
                    <small>Pinned on {{$userTask->created_at}}</small>
                @endforeach
            @else 
                <h5>You have no tasks pinned</h5>
            @endif
        </ol>
    </div>
@endsection
