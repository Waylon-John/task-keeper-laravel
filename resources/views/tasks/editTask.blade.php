@extends('layouts.main')

@section('content')
    <div class="row center section">
        <h4 class="blue-text">Edit Task</h4>
    </div>
    <div class="row">
        {!!Form::open(['action'=>['TasksController@update',$task->id],'method'=>'PUT'])!!}
        <div class="input-field col s10">
            <i class="material-icons prefix">create</i>
            {!!Form::text('task', $task->task)   !!}
        </div>
        <div class="input-field col s2">
            {!!Form::submit('Update Task',['class'=>'blue btn-small'])!!}
        </div>
        {!!Form::close()!!}
    </div>
    <div class="row center">
        <a href="#" class="red btn-small" onclick="history.back()">
                Cancel
        </a>
    </div>
@endsection