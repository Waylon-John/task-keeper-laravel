@extends('layouts.main')

@section('content')
    <div class="container center">
        <h3>Welcome to TasKeeper</h3>
        <h5 class="blue-text section">Login now to start pinning your tasks.</h5>
    </div>
    <div class="container section">
        <div class="container section">
            <form method="POST" action="{{ route('login') }}">
                @csrf

                <div>
                    <label class="red-text" for="email">{{ __('E-Mail Address') }}</label>

                    <div>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div>
                    <label for="password" class="red-text">{{ __('Password') }}</label>

                    <div>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <div>
                        <div>
                            <label>
                                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <span>{{ __('Remember Me') }}</span>
                            </label>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="center">
                        <button type="submit" class="blue btn-small">
                            {{ __('Login') }}
                        </button>
                    </div>
                    <div class="section center">
                        @if (Route::has('password.request'))
                            <a href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif
                    </div>
                </div>
            </form>
        </div>
        <div class="divider"></div>
        <div class="container center">
            <h5><a href="/register">Click here to Register an account!!</a></h5>
        </div>
    </div>
@endsection