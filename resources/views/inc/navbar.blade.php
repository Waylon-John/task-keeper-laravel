<nav>
    <div class="nav-wrapper blue">
      <a href="#" class="brand-logo">TasKeeper</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li><a href="/">Home</a></li>
        @guest
        @else
            <li>
                <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>    
            </li> 
        @endguest
      </ul>
    </div>
  </nav>